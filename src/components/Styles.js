import { Dimensions, StyleSheet } from 'react-native';

var { height } = Dimensions.get('window')

var box_count = 2
var box_height = height / box_count

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: "space-around"
  },
  box: {
    flex: 1,
    height: box_height
  },
  navbar: {
    flex: 1,
    backgroundColor: '#3ec6be',
    alignItems: 'center',
    justifyContent: "center",  
    maxHeight: 60,
    elevation: 5,
    marginBottom: 20,
  },
  navbarText: {
    fontSize: 31,
    letterSpacing: -0.4,
    color: '#E8E8E8',
    fontWeight: "700",
    fontStyle: "normal",
    textTransform: "capitalize"
  },
  tasksContainer: {
    flex: 10,
    backgroundColor: '#e8e8e8',
    alignItems: 'center',
    justifyContent: "center"
  },
  tasksText: {
    fontSize: 31,
    letterSpacing: -0.4,
    color: '#3ec6be',
    fontWeight: "700",
    fontStyle: "normal",
    textTransform: "capitalize"
  },
  textInput: {
    borderWidth: 1,
    borderColor: "red",
    borderRadius: 3
  }
});

export {styles}