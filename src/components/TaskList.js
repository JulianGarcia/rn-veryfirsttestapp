import React, { Component } from 'react';
import { FontVariant, View, Dimensions, StyleSheet, FlatList, Text } from 'react-native';
import TaskItem from "./TaskItem";
import { styles } from "./Styles"


export default class TaskList extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={[styles.box, styles.tasksContainer]}>
          <FlatList style={styles.tasksText}
              data = {this.props.tareas}
              renderItem = {({item}) => <TaskItem tarea={item}/>}
              keyExtractor = {item => String(item.id)}
          />
        </View>
      </View>
    );
  }
}
