import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';




const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: "space-around"
  },
  box: {
    flex: 1,
  },
  taskContainer: {
    flex: 10,
    backgroundColor: '#e8e8e8',
    alignItems: 'center',
    justifyContent: "center"
  },
  taskText: {
    fontSize: 31,
    letterSpacing: -0.4,
    color: '#3ec6be',
    fontWeight: "700",
    fontStyle: "normal",
    textTransform: "capitalize"
  }
});

export default class TaskItem extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    const { tarea } = this.props
    return (
      <View style={styles.box, styles.container}>
        <Text style={styles.taskText}> 
            {tarea.completada ? "completada" : "NO completada"} 
        </Text>
      </View>
    );
  }
}
