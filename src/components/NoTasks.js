import React, { Component } from 'react';
import { View, Text } from 'react-native';

export default class NoTasks extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View>
        <Text> NoTasks -> No tiene tareas para mostrar </Text>
      </View>
    );
  }
}
