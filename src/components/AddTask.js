import React, { Component } from 'react';
import { View, TextInput, Button } from 'react-native';
import { styles } from "./Styles"

export default class AddTask extends Component {
  constructor(props) {
    super(props);
    this.state = { tarea: "" }
  }

  onCancel() {
    this.setState({ tarea: ""})
  }

  onAddTask() {
    const { tarea } = this.state;
    this.props.onAddTask(tarea);
    this.setState({ tarea: ""})
  }
  onTaskInput(text) {
      this.setState({
        tarea: text
      });
  }


  render() {
    return (
      <View>
        <TextInput
            style = { styles.textInput}
            placeholder="Nueva Tarea" onChangeText={
            text => this.onTaskInput(text)}
            value = {this.state.tarea}
        />
        <View>
            <Button 
                title="Cancelar" 
                color="red" 
                onPress={() => this.onCancel()}
            />
            <Button
                title="Aceptar" 
                color="green" 
                onPress={() => this.onAddTask()}
            />
        </View>

      </View>
    );
  }
}
