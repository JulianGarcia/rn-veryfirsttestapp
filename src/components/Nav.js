import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { styles } from "./Styles"

export default class Nav extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
        <View style={[styles.box, styles.navbar]}>
            <Text style={styles.navbarText}>ToDo App!</Text>
        </View>
    );
  }
}
