import React, { Component } from 'react';
import { View, Text } from 'react-native';
import NoTasks from "./src/components/NoTasks"
import TaskList from "./src/components/TaskList"
import AddTask from "./src/components/AddTask"
import Nav from "./src/components/Nav"


export default class App extends Component {

  constructor(){
    super()
    this.state = {
      tareas: [
        { id: 0, titulo: "Una tarea", completada: false},
        { id: 1, titulo: "otra tarea", completada: false},
        { id: 2, titulo: "tarea 5", completada: true},
        { id: 3, titulo: "tarea 4", completada: false},
        { id: 4, titulo: "tarea 5", completada: true}
      ]
    }
  }

  onAddTask(tarea) {
    alert("se agrego una tarea" + tarea)
  }
  
  render(){
    return(
      <View style={{flex: 1}}>
        <Nav/>
        <AddTask onAddTask={tarea => this.onAddTask(tarea)}/>
        {!this.state.tareas || this.state.tareas.length === 0 ? (
          <NoTasks />
        ) : (
          <TaskList tareas={this.state.tareas}/>
        )  
      
      }
      </View>
    )
  }

  
}
